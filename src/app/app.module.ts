import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TimePickerComponent } from './time-picker/time-picker.component';
import { OwsTimePickerComponent } from './ows-time-picker/ows-time-picker.component';

@NgModule({
  declarations: [
    AppComponent,
    TimePickerComponent,
    OwsTimePickerComponent
  ],
  entryComponents:[TimePickerComponent],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, ViewChild, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { TimePickerComponent } from '../time-picker/time-picker.component';

@Component({
  selector: 'ows-time-picker',
  templateUrl: './ows-time-picker.component.html',
  styleUrls: ['./ows-time-picker.component.scss']
})

export class OwsTimePickerComponent {

  @ViewChild("container", {read: ViewContainerRef}) container;
  
  constructor(private resolver: ComponentFactoryResolver){}

  set(ele){
    const testComponent = this.resolver.resolveComponentFactory(TimePickerComponent);
    var tsc = this.container.createComponent(testComponent);
    tsc.instance._ref = tsc;
    tsc.instance.timerElement = ele;
    tsc.instance.selectedTime.subscribe(time => {
      ele.value = time;
    })
  }
}
